﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HomeWork_for_Present_Connection.Models;

namespace HomeWork_for_Present_Connection.Controllers
{
    public class CompetenciesController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: Competencies
        public ActionResult Index(int OrganisationId = 0)
        {
            var competencies = db.Competencies.Include(c => c.Organisation);
            //return View(competencies.ToList());

            //var competence = db.Competence.Include(c => c.User).Include(c => c.Competency);
            //var competenceAssessments = db.CompetenceAssessments.Include(c => c.User).Include(c => c.Competency);
            ViewBag.OrganisationId = new SelectList(db.Organisations, "Id", "Name");
            var competence = db.Competencies.Include(c => c.Organisation);
            if (OrganisationId > 0)
            {
                var model =
                    from r in db.Competencies
                    orderby r.Name
                    where r.Organisation.Id == OrganisationId //r.User.Organisation.Id == OrganisationId
                    select r;
                return View(model);
            }
            else
            {
                var model = db.Competencies.Include(c => c.Organisation);
                return View(model);
            }
        }

        // GET: Competencies/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Competency competency = db.Competencies.Find(id);
            if (competency == null)
            {
                return HttpNotFound();
            }
            return View(competency);
        }

        // GET: Competencies/Create
        public ActionResult Create()
        {
            ViewBag.OrganisationId = new SelectList(db.Organisations, "Id", "Name");
            return View();
        }

        // POST: Competencies/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Name,Status,OrganisationId")] Competency competency)
        {
            if (ModelState.IsValid)
            {
                db.Competencies.Add(competency);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.OrganisationId = new SelectList(db.Organisations, "Id", "Name", competency.OrganisationId);
            return View(competency);
        }

        // GET: Competencies/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Competency competency = db.Competencies.Find(id);
            if (competency == null)
            {
                return HttpNotFound();
            }
            ViewBag.OrganisationId = new SelectList(db.Organisations, "Id", "Name", competency.OrganisationId);
            return View(competency);
        }

        // POST: Competencies/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Name,Status,OrganisationId")] Competency competency)
        {
            if (ModelState.IsValid)
            {
                db.Entry(competency).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.OrganisationId = new SelectList(db.Organisations, "Id", "Name", competency.OrganisationId);
            return View(competency);
        }

        // GET: Competencies/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Competency competency = db.Competencies.Find(id);
            if (competency == null)
            {
                return HttpNotFound();
            }
            return View(competency);
        }

        // POST: Competencies/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Competency competency = db.Competencies.Find(id);
            db.Competencies.Remove(competency);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
