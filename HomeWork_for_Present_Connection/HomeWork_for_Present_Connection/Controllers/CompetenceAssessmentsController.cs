﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HomeWork_for_Present_Connection.Models;

namespace HomeWork_for_Present_Connection.Controllers
{
    public class CompetenceAssessmentsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();

        // GET: CompetenceAssessments
        public ActionResult Index(int OrganisationId = 0)
        {
            ViewBag.OrganisationId = new SelectList(db.Organisations, "Id", "Name");
            var competenceAssessments = db.CompetenceAssessments.Include(c => c.User).Include(c => c.Competency);
            if (OrganisationId > 0)
            {
                var model =
                    from r in db.CompetenceAssessments
                    orderby r.CreationDate
                    where r.User.Organisation.Id == OrganisationId
                    select r;
                return View(model);
            }
            else
            {
                var model = db.CompetenceAssessments.Include(c => c.User).Include(c => c.Competency);
                return View(model);
            }
            
        }

        // GET: CompetenceAssessments/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompetenceAssessment competenceAssessment = db.CompetenceAssessments.Find(id);
            if (competenceAssessment == null)
            {
                return HttpNotFound();
            }
            return View(competenceAssessment);
        }

        // GET: CompetenceAssessments/Create
        public ActionResult Create()
        {
            ViewBag.UserId = new SelectList(db.Users, "Id", "FirstName");
            ViewBag.CompetencyId = new SelectList(db.Competencies, "Id", "Name");
            return View();
        }

        // POST: CompetenceAssessments/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Id,Assessment,CreationDate,UserId,CompetencyId")] CompetenceAssessment competenceAssessment)
        {
            if (ModelState.IsValid)
            {
                db.CompetenceAssessments.Add(competenceAssessment);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.UserId = new SelectList(db.Users, "Id", "FirstName", competenceAssessment.UserId);
            ViewBag.CompetencyId = new SelectList(db.Competencies, "Id", "Name", competenceAssessment.CompetencyId);
            return View(competenceAssessment);
        }

        // GET: CompetenceAssessments/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompetenceAssessment competenceAssessment = db.CompetenceAssessments.Find(id);
            if (competenceAssessment == null)
            {
                return HttpNotFound();
            }
            ViewBag.UserId = new SelectList(db.Users, "Id", "FirstName", competenceAssessment.UserId);
            ViewBag.CompetencyId = new SelectList(db.Competencies, "Id", "Name", competenceAssessment.CompetencyId);
            return View(competenceAssessment);
        }

        // POST: CompetenceAssessments/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Id,Assessment,CreationDate,UserId,CompetencyId")] CompetenceAssessment competenceAssessment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(competenceAssessment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.UserId = new SelectList(db.Users, "Id", "FirstName", competenceAssessment.UserId);
            ViewBag.CompetencyId = new SelectList(db.Competencies, "Id", "Name", competenceAssessment.CompetencyId);
            return View(competenceAssessment);
        }

        // GET: CompetenceAssessments/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CompetenceAssessment competenceAssessment = db.CompetenceAssessments.Find(id);
            if (competenceAssessment == null)
            {
                return HttpNotFound();
            }
            return View(competenceAssessment);
        }

        // POST: CompetenceAssessments/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CompetenceAssessment competenceAssessment = db.CompetenceAssessments.Find(id);
            db.CompetenceAssessments.Remove(competenceAssessment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
