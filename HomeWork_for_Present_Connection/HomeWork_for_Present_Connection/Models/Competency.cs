using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HomeWork_for_Present_Connection.Models
{

    
    public partial class Competency
    {
       
        public Competency()
        {
            this.CompetenceAssessments = new HashSet<CompetenceAssessment>();
        }
    
        public int Id { get; set; }

        [Display(Name = "Kompetencija")]
        public string Name { get; set; }

        [Display(Name = "Kompetencijos aprasas")]
        public string Status { get; set; }

        public int OrganisationId { get; set; }
    
        
        public virtual ICollection<CompetenceAssessment> CompetenceAssessments { get; set; }
        public virtual Organisation Organisation { get; set; }
    }
}
