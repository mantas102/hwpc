using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HomeWork_for_Present_Connection.Models
{

    
    public partial class User
    {
        
        public User()
        {
            this.CompetenceAssessments = new HashSet<CompetenceAssessment>();
        }
    
        public int Id { get; set; }

        [Display(Name = "Vardas")]
        public string FirstName { get; set; }

        [Display(Name = "Pavarde")]
        public string LastName { get; set; }

        [Display(Name = "El-pastas")]
        public string Email { get; set; }

        [Display(Name = "Tel. numeris")]
        public string PhoneNumber { get; set; }

        public DateTime RegistrationDate
        {
            get
            {
                return this.registrationDate.HasValue
                    ? this.registrationDate.Value
                    : DateTime.Now;
            }
            set
            {
                this.registrationDate = value;
            }
        }
        private DateTime? registrationDate = null;

        [Display(Name = "Darbuotojas")]
        public string Name
        {
            get
            {
                return string.Format("{0} {1}", this.FirstName, this.LastName);
            }
        }



        public int OrganisationId { get; set; }
    
        public virtual Organisation Organisation { get; set; }
        
        public virtual ICollection<CompetenceAssessment> CompetenceAssessments { get; set; }
    }
}
