using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace HomeWork_for_Present_Connection.Models
{

    
    public partial class CompetenceAssessment
    {
        public int Id { get; set; }

        [Display(Name = "Vertinimas")]
        public string Assessment { get; set; }

        [Display(Name = "Data")]
        public DateTime CreationDate
        {
            get
            {
                return this.creationDate.HasValue
                    ? this.creationDate.Value
                    : DateTime.Now;
            }
            set
            {
                this.creationDate = value;
            }
        }
        private DateTime? creationDate = null;

        public int UserId { get; set; }

        public int CompetencyId { get; set; }
    
        public virtual User User { get; set; }
        public virtual Competency Competency { get; set; }
    }
}
