﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace HomeWork_for_Present_Connection.Models
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() : base("name=DefaultConnection")
        {
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        
        public virtual DbSet<Organisation> Organisations { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Competency> Competencies { get; set; }
        public virtual DbSet<CompetenceAssessment> CompetenceAssessments { get; set; }
        

    }
}