using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace HomeWork_for_Present_Connection.Models
{
    
    
    public partial class Organisation
    {
        
        public Organisation()
        {
            this.Users = new HashSet<User>();
            this.Competencies = new HashSet<Competency>();
        }
    
        public int Id { get; set; }

        [Display(Name = "Pavadinimas")]
        public string Name { get; set; }

        [Display(Name = "Aprasymas")]
        public string Description { get; set; }

        [Display(Name = "Statusas")]
        public string Status { get; set; }

        public int ParentID { get; set; }

        public DateTime CreationDate
        {
            get
            {
                return this.creationDate.HasValue
                    ? this.creationDate.Value
                    : DateTime.Now;
            }
            set
            {
                this.creationDate = value;
            }
        }
        private DateTime? creationDate = null;
    
        
        public virtual ICollection<User> Users { get; set; }
        
        public virtual ICollection<Competency> Competencies { get; set; }
    }
}
