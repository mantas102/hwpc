
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 03/25/2016 15:33:00
-- Generated from EDMX file: C:\Users\Mantas Kalanta\documents\visual studio 2015\Projects\HomeWork_for_Present_Connection\HomeWork_for_Present_Connection\Models\DbModel.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [LocalDB_for_PC];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_OrganisationUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Users] DROP CONSTRAINT [FK_OrganisationUser];
GO
IF OBJECT_ID(N'[dbo].[FK_UserCompetenceAssessment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CompetenceAssessments] DROP CONSTRAINT [FK_UserCompetenceAssessment];
GO
IF OBJECT_ID(N'[dbo].[FK_CompetencyCompetenceAssessment]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[CompetenceAssessments] DROP CONSTRAINT [FK_CompetencyCompetenceAssessment];
GO
IF OBJECT_ID(N'[dbo].[FK_OrganisationCompetency]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[Competencies] DROP CONSTRAINT [FK_OrganisationCompetency];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[Organisations]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Organisations];
GO
IF OBJECT_ID(N'[dbo].[Users]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Users];
GO
IF OBJECT_ID(N'[dbo].[Competencies]', 'U') IS NOT NULL
    DROP TABLE [dbo].[Competencies];
GO
IF OBJECT_ID(N'[dbo].[CompetenceAssessments]', 'U') IS NOT NULL
    DROP TABLE [dbo].[CompetenceAssessments];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'Organisations'
CREATE TABLE [dbo].[Organisations] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Description] nvarchar(max)  NOT NULL,
    [Status] nvarchar(max)  NOT NULL,
    [ParentID] int  NOT NULL,
    [CreationDate] datetime  NOT NULL
);
GO

-- Creating table 'Users'
CREATE TABLE [dbo].[Users] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [FirstName] nvarchar(max)  NOT NULL,
    [LastName] nvarchar(max)  NOT NULL,
    [Email] nvarchar(max)  NOT NULL,
    [PhoneNumber] nvarchar(max)  NOT NULL,
    [RegistrationDate] datetime  NOT NULL,
    [OrganisationId] int  NOT NULL
);
GO

-- Creating table 'Competencies'
CREATE TABLE [dbo].[Competencies] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Status] nvarchar(max)  NOT NULL,
    [OrganisationId] int  NOT NULL
);
GO

-- Creating table 'CompetenceAssessments'
CREATE TABLE [dbo].[CompetenceAssessments] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Assessment] nvarchar(max)  NOT NULL,
    [CreationDate] datetime  NOT NULL,
    [UserId] int  NOT NULL,
    [CompetencyId] int  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Id] in table 'Organisations'
ALTER TABLE [dbo].[Organisations]
ADD CONSTRAINT [PK_Organisations]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [PK_Users]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'Competencies'
ALTER TABLE [dbo].[Competencies]
ADD CONSTRAINT [PK_Competencies]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'CompetenceAssessments'
ALTER TABLE [dbo].[CompetenceAssessments]
ADD CONSTRAINT [PK_CompetenceAssessments]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [OrganisationId] in table 'Users'
ALTER TABLE [dbo].[Users]
ADD CONSTRAINT [FK_OrganisationUser]
    FOREIGN KEY ([OrganisationId])
    REFERENCES [dbo].[Organisations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrganisationUser'
CREATE INDEX [IX_FK_OrganisationUser]
ON [dbo].[Users]
    ([OrganisationId]);
GO

-- Creating foreign key on [UserId] in table 'CompetenceAssessments'
ALTER TABLE [dbo].[CompetenceAssessments]
ADD CONSTRAINT [FK_UserCompetenceAssessment]
    FOREIGN KEY ([UserId])
    REFERENCES [dbo].[Users]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserCompetenceAssessment'
CREATE INDEX [IX_FK_UserCompetenceAssessment]
ON [dbo].[CompetenceAssessments]
    ([UserId]);
GO

-- Creating foreign key on [CompetencyId] in table 'CompetenceAssessments'
ALTER TABLE [dbo].[CompetenceAssessments]
ADD CONSTRAINT [FK_CompetencyCompetenceAssessment]
    FOREIGN KEY ([CompetencyId])
    REFERENCES [dbo].[Competencies]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_CompetencyCompetenceAssessment'
CREATE INDEX [IX_FK_CompetencyCompetenceAssessment]
ON [dbo].[CompetenceAssessments]
    ([CompetencyId]);
GO

-- Creating foreign key on [OrganisationId] in table 'Competencies'
ALTER TABLE [dbo].[Competencies]
ADD CONSTRAINT [FK_OrganisationCompetency]
    FOREIGN KEY ([OrganisationId])
    REFERENCES [dbo].[Organisations]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_OrganisationCompetency'
CREATE INDEX [IX_FK_OrganisationCompetency]
ON [dbo].[Competencies]
    ([OrganisationId]);
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------