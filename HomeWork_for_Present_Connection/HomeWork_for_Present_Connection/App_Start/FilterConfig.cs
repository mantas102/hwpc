﻿using System.Web;
using System.Web.Mvc;

namespace HomeWork_for_Present_Connection
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
